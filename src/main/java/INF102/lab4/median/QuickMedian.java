package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        int middle = listCopy.size() / 2;
        return quickSelect(listCopy, middle);
    }

    Random random = new Random();

    public <T extends Comparable<? super T>> T quickSelect(List<T> list, int k) {
        if (list.size() <= 0) throw new IllegalArgumentException("Empty list");
        if (list.size() == 1) return list.get(0);

        T pivot = list.get(random.nextInt(list.size()));
        List<T> first = new ArrayList<>();
        List<T> last = new ArrayList<>();
        List<T> pivots = new ArrayList<>();

        for (T t : list) {
            int cmp = t.compareTo(pivot);

            if (cmp < 0) first.add(t);
            else if (cmp > 0) last.add(t);
            else pivots.add(t);
        }

        if (k < first.size()) return quickSelect(first, k);
        else if (k < first.size() + pivots.size()) return pivots.get(0);
        else return quickSelect(last, k - first.size() - pivots.size());
    }
}
