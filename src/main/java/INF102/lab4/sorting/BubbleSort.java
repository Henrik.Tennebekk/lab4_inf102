package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        
        int listSize = list.size();
        
        for (int i = 0; i < listSize - 1; i++) { // O(n)
            for (int j = 0; j < (listSize - i - 1); j++) { // O(n)
                if (list.get(j).compareTo(list.get(j + 1)) > 0) { // O(1)

                    T temp = list.get(j);
                    list.set(j, list.get(j+1));
                    list.set(j+1, temp);
                }
            }
        }
    }
    
}
